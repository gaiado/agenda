<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Password Reset Language Lines
    |--------------------------------------------------------------------------
    |
    | Estos son los textos usados para los toas de primevue
    |
    */

    'roles' => [
        'stored' => 'Rol guardado con éxito',
        'updated' => 'Rol actualizado con éxito',
        'deleted' => 'Rol eliminado con éxito'
    ],
    'modules' => [
        'stored' => 'Módulo guardado con éxito',
        'updated' => 'Módulo actualizado con éxito',
        'deleted' => 'Módulo eliminado con éxito'
    ],
    'permissions' => [
        'stored' => 'Permiso guardado con éxito',
        'updated' => 'Permiso actualizado con éxito',
        'deleted' => 'Permiso eliminado con éxito'
    ],
    'users' => [
        'stored' => 'Usuario guardado con éxito',
        'updated' => 'Usuario actualizado con éxito',
        'deleted' => 'Usuario eliminado con éxito'
    ],
];
