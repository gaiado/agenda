<?php

namespace App\Http\Requests;

use Illuminate\Validation\Rule;
use Illuminate\Foundation\Http\FormRequest;

class UpdateRoleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $id = $this->route('role')->id;
        return [
            'label' => ['required', 'max:50'],
            'name' => ['required', 'max:50', Rule::unique('roles')->ignore($id)],
            'guard_name' => 'required|max:50',
            'description' => 'max:255',
            'permissions' => 'array',
            'permissions.*' => 'exists:permissions,name',
        ];
    }
}
