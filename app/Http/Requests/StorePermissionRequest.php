<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Str;

class StorePermissionRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     */
    public function authorize(): bool
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, \Illuminate\Contracts\Validation\ValidationRule|array<mixed>|string>
     */
    public function rules(): array
    {
        $module_name = $this->route('module')->name;
        return [
            'module_id' => 'exists:modules,id',
            'label' => 'required|max:50',
            'name' => ['required', "starts_with:$module_name", 'unique:permissions', 'max:50'],
            'guard_name' => 'required|max:50',
            'description' => 'max:255',
        ];
    }

    /**
     * Prepare the data for validation.
     */
    protected function prepareForValidation(): void
    {
        $module_id = $this->route('module')->id;
        $this->merge([
            'module_id' => $module_id,
            'name' => Str::slug($this->name),
        ]);
    }
}
