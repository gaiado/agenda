<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePermissionRequest;
use App\Models\Role;
use Inertia\Inertia;
use App\Http\Requests\StoreRoleRequest;
use Illuminate\Support\Facades\Request;
use App\Http\Requests\UpdateRoleRequest;
use App\Models\Module;

class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $items = Role::when(Request::input('search'), function ($query, $search) {
            $query->whereAny([
                'name',
                'description'
            ], 'LIKE', "%$search%");
        })
            ->paginate(10)
            ->withQueryString();
        return Inertia::render('Roles/List', [
            'items' => $items,
            'filters' => Request::only(['search'])
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $modules = $this->getAllModules();
        return Inertia::render('Roles/Create', [
            'modules' => $modules,
        ]);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreRoleRequest $request)
    {
        $validated = $request->validated();
        $role = Role::create($validated);
        $role->syncPermissions([$validated['permissions']]);
        return to_route('roles.index')->with('success', __('toast.roles.stored'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Role $role)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Role $role)
    {
        $modules = $this->getAllModules();
        $permissions = $role->permissions()->pluck('name');
        return Inertia::render('Roles/Create', [
            'model' => $role,
            'permissions' => $permissions,
            'modules' => $modules
        ]);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateRoleRequest $request, Role $role)
    {
        $validated = $request->validated();
        $role->fill($validated);
        $role->save();
        $role->syncPermissions([$validated['permissions']]);
        return to_route('roles.index')->with('success', __('toast.roles.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Role $role)
    {
        $role->delete();
        return to_route('roles.index')->with('success', __('toast.roles.deleted'));
    }
    /**
     * Confirm delete specified resource from storage.
     */
    public function delete(Role $role)
    {
        return Inertia::modal('Roles/Delete', [
            'model' => $role
        ])->baseRoute('modules.index');
    }

    /**
     * Get available modules and permissions.
     */
    private function getAllModules()
    {
        return Module::has('permissions')->with('permissions')->get();
    }
}
