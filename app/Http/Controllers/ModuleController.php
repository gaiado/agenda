<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Module;
use Illuminate\Support\Facades\Request;
use App\Http\Requests\StoreModuleRequest;
use App\Http\Requests\UpdateModuleRequest;

class ModuleController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $items = Module::when(Request::input('search'), function ($query, $search) {
            $query->whereAny([
                'name',
                'description'
            ], 'LIKE', "%$search%");
        })
            ->paginate(10)
            ->withQueryString();
        return Inertia::render('Modules/List', [
            'items' => $items,
            'filters' => Request::only(['search'])
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return Inertia::modal('Modules/Create', [])->baseRoute('modules.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreModuleRequest $request)
    {
        $validated = $request->validated();
        Module::create($validated);
        return redirect(route('modules.index'))->with('success', __('toast.modules.stored'));;
    }

    /**
     * Display the specified resource.
     */
    public function show(Module $module)
    {
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Module $module)
    {
        return Inertia::modal('Modules/Create', [
            'model' => $module
        ])->baseRoute('modules.index');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateModuleRequest $request, Module $module)
    {
        $validated = $request->validated();
        $module->fill($validated);
        $module->save();
        return to_route('modules.index')->with('success', __('toast.modules.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Module $module)
    {
        $module->delete();
        return to_route('modules.index')->with('success', __('toast.modules.deleted'));
    }

    /**
     * Confirm delete specified resource from storage.
     */
    public function delete(Module $module)
    {
        return Inertia::modal('Modules/Delete', [
            'model' => $module
        ])->baseRoute('modules.index');
    }
}
