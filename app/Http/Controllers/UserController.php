<?php

namespace App\Http\Controllers;

use App\Models\User;
use Inertia\Inertia;
use App\Http\Requests\StoreUserRequest;
use Illuminate\Support\Facades\Request;
use App\Http\Requests\UpdateUserRequest;
use App\Models\Role;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $items = User::when(Request::input('search'), function ($query, $search) {
            $query->whereAny([
                'name',
                'email'
            ], 'LIKE', "%$search%");
        })
            ->with(['roles:label,name'])
            ->paginate(10)
            ->withQueryString();
        return Inertia::render('Users/List', [
            'items' => $items,
            'filters' => Request::only(['search'])
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        $roles = $this->getAllRoles();
        return Inertia::modal('Users/Create', [
            'roles' => $roles
        ])->baseRoute('users.index');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StoreUserRequest $request)
    {
        $validated = $request->validated();
        $user = User::create($validated);
        $user->syncRoles([$validated['role_id']]);
        return to_route('users.index')->with('success', __('toast.users.stored'));
    }

    /**
     * Display the specified resource.
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(User $user)
    {
        $roles = $this->getAllRoles();
        return Inertia::modal('Users/Create', [
            'model' => $user,
            'roles' => $roles
        ])->baseRoute('users.index');
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdateUserRequest $request, User $user)
    {
        $validated = $request->validated();
        $user->fill($validated);
        $user->syncRoles([$validated['role_id']]);
        return to_route('users.index')->with('success', __('toast.users.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(User $user)
    {
        $user->delete();
        return to_route('users.index')->with('success', __('toast.users.deleted'));
    }

    /**
     * Confirm delete specified resource from storage.
     */
    public function delete(User $user)
    {
        return Inertia::modal('Users/Delete', [
            'model' => $user
        ])->baseRoute('users.index');
    }

    /**
     * Get available roles.
     */
    private function getAllRoles()
    {
        return Role::select('name', 'label')->get();
    }
}
