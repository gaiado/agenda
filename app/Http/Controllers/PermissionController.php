<?php

namespace App\Http\Controllers;

use Inertia\Inertia;
use App\Models\Module;
use App\Models\Permission;
use Illuminate\Support\Facades\Request;
use App\Http\Requests\StorePermissionRequest;
use App\Http\Requests\UpdatePermissionRequest;

class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index(Module $module)
    {
        $items = $module->permissions()->when(Request::input('search'), function ($query, $search) {
            $query->whereAny([
                'name',
                'description'
            ], 'LIKE', "%$search%");
        })
            ->paginate(10)
            ->withQueryString();
        return Inertia::render('Permissions/List', [
            'model' => $module,
            'items' => $items,
            'filters' => Request::only(['search'])
        ]);
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create(Module $module)
    {
        return Inertia::modal('Permissions/Create', [
            'module' => $module
        ])->baseRoute('modules.permissions.index', $module);
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(StorePermissionRequest $request, Module $module)
    {
        $validated = $request->validated();
        Permission::create($validated);
        return to_route('modules.permissions.index', $module)->with('success', __('toast.permissions.stored'));
    }

    /**
     * Display the specified resource.
     */
    public function show(Permission $permission)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(Module $module, Permission $permission)
    {
        return Inertia::modal('Permissions/Create', [
            'module' => $module,
            'model' => $permission
        ])->baseRoute('modules.permissions.index', $module);
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(UpdatePermissionRequest $request,  Module $module, Permission $permission)
    {
        $validated = $request->validated();
        $permission->fill($validated);
        $permission->save();
        return to_route('modules.permissions.index', $module)->with('success', __('toast.permissions.updated'));
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(Module $module, Permission $permission)
    {
        $permission->delete();
        return to_route('modules.permissions.index', $module)->with('success', __('toast.permissions.deleted'));
    }

    /**
     * Confirm delete specified resource from storage.
     */
    public function delete(Module $module, Permission $permission)
    {
        return Inertia::modal('Permissions/Delete', [
            'model' => $permission,
            'module' => $module
        ])->baseRoute('modules.permissions.index', $module);
    }
}
